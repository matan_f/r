setwd("C:/Users/matan/Desktop/??????????????/?????? ??/?????????? ??/?????????? ??????/???????????? ??????")
house.org<-read.csv("kc_house_data.csv")
house<-house.org

library(dplyr)
library(ggplot2)
library(caTools)
library(lubridate)

str(house)
summary(house)
head(house)

str(as.factor(house$id))
house$id<- NULL
house$try<-c(1:21613)
house$try<-NULL

#Date- year, month
house$year<- substr(house$date,1,4)
house$month<- substr(house$date,5,6)
table(house$month)
table(house$year)

house$month<-as.numeric(house$month)
breaks<-c(0,6,12)
bins<-cut(house$month, breaks, include.lowest = F, right = T, labels=c('Half1','Half2'))
table(bins)
house$half<-bins

house$yr_renovated<-house.org$yr_renovated
yeRenovatedDeal<- function(x){
  if(x==0){
    return(('Not revonted'))
  }else{
    return(('revonted'))
  }
}

house$yr_renovated<-sapply(house$yr_renovated,yeRevontedDeal)
house$yr_renovated<-as.factor(house$yr_renovated)
str(house$yr_renovated)

house$date<-NULL
ggplot(house, aes(price))+geom_histogram()

#bedrooms
ggplot(house, aes(bedrooms,price))+geom_point()+stat_smooth()


ggplot(house, aes(yr_renovated,price))+geom_boxplot()+ylim(0,3000000)

filter <- sample.split(house$price, SplitRatio = 0.7)

#Training set 
house.train <- subset(house, filter==TRUE)

#test set 
house.test <- subset(house, filter==F)

#creating a model
model <- lm(price ~ .,house.train )
summary(model)

predict.train <- predict(model,house.train)
predict.test  <- predict(model,house.test)

mse.train<-mean((house.train$price- predict.train)**2)
mse.test<-mean((house.test$price- predict.test)**2)

me.train<- mse.train**0.5
me.test<-mse.test**0.5


