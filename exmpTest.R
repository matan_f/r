#install.packages('dplyr')
library(dplyr)
#install.packages('pROC')
library(pROC)
library(ggplot2)
library(caTools)
library(lubridate)

#1.1
setwd("C:/Users/matan/Desktop/??????????????/?????? ??/?????????? ??/?????????? ??????")
customer.raw<-read.csv("test1.csv")

#1.2
str(customer.raw)
summary(customer.raw)
#ID, paperlessBilling
customer<-customer.raw
customer$customerID<-NULL

#1.3- seniorCitizen
customer$SeniorCitizen<-factor(customer$SeniorCitizen, levels = 0:1, labels = c('not senior','senior citizen'))
str(customer)

#2.1
ggplot(customer, aes(tenure , fill = Churn)) + geom_histogram(position = 'fill')
# 0 means that is a new customer, so he didn't had the chance to leave- those records are irrelevant
customer <- (customer %>% filter(tenure != 0))

#2.2
ggplot(customer, aes(gender, fill = Churn)) + geom_bar(position = 'fill')
# its seems that gender has no effect on churn

#2.3
ggplot(customer, aes(Contract, fill = Churn)) + geom_bar(position = 'fill')
# yes

#2.4
summary(customer)
c<-table(customer$PaperlessBilling, customer$Churn)
precision<-c['Yes','Yes']/(c['Yes','Yes']+c['Yes','No'])
recall<-c['Yes','Yes']/(c['Yes','Yes']+c['No','Yes'])

#3
filter <- sample.split(customer$Churn, SplitRatio = 0.7)

cust.train <- subset(customer, filter == TRUE)
cust.test <- subset(customer, filter == F)

dim(customer)
dim(cust.train)
dim(cust.test)

##cust.train.dt<- cust.train[,c("TechSupport", "tenure", "Contract","Churn")]
model.dt <- rpart(Churn ~ TechSupport+ tenure+ Contract , cust.train)
rpart.plot(model.dt, box.palette = "RdBu", shadow.col = "gray", nn = TRUE)

prediction <- predict(model.dt,cust.test)
predict.prob.yes <- prediction[,'Yes']
prediciton.dt <- predict.prob.yes > 0.5
actual <- cust.test$Churn

#cf <- table(actual,prediciton.dt > 0.5)
#precision <- cf['Yes','TRUE']/(cf['Yes','TRUE'] + cf['No','TRUE'])
#recall <- cf['Yes','TRUE']/(cf['Yes','TRUE'] + cf['Yes','FALSE'])
cf <- table(prediciton.dt > 0.5, actual)
precision <-cf['TRUE','Yes']/(cf['TRUE','Yes'] + cf['TRUE','No'] )
recall <- cf['TRUE','Yes']/(cf['TRUE','Yes'] + cf['FALSE','Yes'] )


errors<- (cf['TRUE','No']+cf['FALSE','Yes'])/dim(cust.test)[1]

#computing base level 
number.churn <- dim(customer[customer$Churn=='Yes',])[1]
base_level <- number.churn/dim(customer)[1]



#4
cust.model <- glm(Churn ~., family = binomial(link = "logit"), data = cust.train)
summary(cust.model)

predicted.cust.test <- predict(cust.model, newdata = cust.test, type = 'response')

confusuon_matrix <- table(predicted.cust.test>0.5, cust.test$Churn )
presicion<-confusuon_matrix[2,2]/(confusuon_matrix[2,2]+confusuon_matrix[2,1])
recall<-confusuon_matrix[2,2]/(confusuon_matrix[2,2]+confusuon_matrix[1,2])
error<-(confusuon_matrix[1,2]+confusuon_matrix[2,1])/dim(cust.test)[1]

rocCurveDC <- roc(actual,predict.prob.yes, direction = ">", levels = c("Yes","No"))
rocCurveRF <- roc(actual,predicted.cust.test, direction = ">", levels = c("Yes","No"))

#plot the chart 
plot(rocCurveDC, col = 'red',main = "ROC Chart")
par(new = TRUE)
plot(rocCurveRF, col = 'blue',main = "ROC Chart")

auc(rocCurveDC)
auc(rocCurveRF)

#if customer conservation is costly we will prefer the model with more precison
#We will call less customers and make more sales 
#Since both models have more or less the same precision we will take the model with better recall

